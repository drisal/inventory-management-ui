import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Inventory } from './Inventory';
import { environment } from 'src/environments/environment';

@Injectable({providedIn: 'root'})
export class InventoryService {
  private apiServerUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient){}

  public getInventories(): Observable<Inventory[]> {
    return this.http.get<Inventory[]>(`${this.apiServerUrl}/inventories/all`);
  }

  public addInventory(inventory: Inventory): Observable<Inventory> {
    return this.http.post<Inventory>(`${this.apiServerUrl}/inventories/add`, inventory);
  }

  public updateInventory(inventory: Inventory): Observable<Inventory> {
    return this.http.put<Inventory>(`${this.apiServerUrl}/inventories/update`, inventory);
  }

  public deleteInventory(itemId: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServerUrl}/inventories/delete/${itemId}`);
  }
}
