import { Component, OnInit } from '@angular/core';
import { Inventory } from './Inventory';
import { InventoryService } from './inventory.service';
import { HttpErrorResponse } from '@angular/common/http';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public inventories: Inventory[];
  public editInventory: Inventory;
  public deleteInventory: Inventory;

  constructor(private inventoryService: InventoryService){}

  ngOnInit() {
    this.getInventories();
  }

  public getInventories(): void {
    this.inventoryService.getInventories().subscribe(
      (response: Inventory[]) => {
        this.inventories = response;
        console.log(this.inventories);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public onAddInventory(addForm: NgForm): void {
    document.getElementById('add-inventory-form').click();
    this.inventoryService.addInventory(addForm.value).subscribe(
      (response: Inventory) => {
        console.log(response);
        this.getInventories();
        addForm.reset();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        addForm.reset();
      }
    );
  }

  public onUpdateInventory(inventory: Inventory): void {
    this.inventoryService.updateInventory(inventory).subscribe(
      (response: Inventory) => {
        console.log(response);
        this.getInventories();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public onDeleteInventory(inventoryId: number): void {
    this.inventoryService.deleteInventory(inventoryId).subscribe(
      (response: void) => {
        console.log(response);
        this.getInventories();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public searchInventories(key: string): void {
    console.log(key);
    const results: Inventory[] = [];
    for (const inventory of this.inventories) {
      if (inventory.name.toLowerCase().indexOf(key.toLowerCase()) !== -1
      || inventory.category.toLowerCase().indexOf(key.toLowerCase()) !== -1
      || inventory.vendor.toLowerCase().indexOf(key.toLowerCase()) !== -1
      || inventory.price.toLowerCase().indexOf(key.toLowerCase()) !== -1) {
        results.push(inventory);
      }
    }
    this.inventories = results;
    if (results.length === 0 || !key) {
      this.getInventories();
    }
  }

  public onOpenModal(inventory: Inventory, mode: string): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');
    if (mode === 'add') {
      button.setAttribute('data-target', '#addInventoryModal');
    }
    if (mode === 'edit') {
      this.editInventory = inventory;
      button.setAttribute('data-target', '#updateInventoryModal');
    }
    if (mode === 'delete') {
      this.deleteInventory = inventory;
      button.setAttribute('data-target', '#deleteInventoryModal');
    }
    container.appendChild(button);
    button.click();
  }



}
