export interface Inventory {
  id: number;
  name: string;
  price: string;
  category: string;
  vendor: string;
  imageUrl: string;
  inventoryCode: string;
}
